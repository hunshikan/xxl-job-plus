# xxl-job-plus

#### 介绍
通过改造xxl-job，实现分布式延迟任务的功能。调度时间精确，误差不超过1秒。

#### 软件架构
软件架构说明


#### 安装教程

1.  xxxx
2.  xxxx
3.  xxxx

#### 使用说明
1、新建表xxl_delay_info，sql在doc文件夹的sql文件中；

2、使用@XxlDelay注解标记处理延迟任务的业务逻辑方法；

3、使用XxlJobDelayHelper#pushDelayTask静态方法发布延迟任务；

4、使用XxlJobDelayHelper#cancel静态方法取消延迟任务；

#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request


#### 特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  Gitee 官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解 Gitee 上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是 Gitee 最有价值开源项目，是综合评定出的优秀开源项目
5.  Gitee 官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  Gitee 封面人物是一档用来展示 Gitee 会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
